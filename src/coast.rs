#[derive(Clone, Eq, PartialEq, Hash)]
pub enum Coast {
    Wing,
    Land,
    Single,
    North,
    South,
    East,
    West,
}

impl Coast {
    pub fn parse(input: &str) -> Option<Coast> {
        match input {
            "wx" => Some(Coast::Wing),
            "mv" => Some(Coast::Land),
            "xc" => Some(Coast::Single),
            "nc" => Some(Coast::North),
            "sc" => Some(Coast::South),
            "wc" => Some(Coast::West),
            "ec" => Some(Coast::East),
            _ => None
        }
    }
    pub fn default_coast(&self)-> Coast{
        match self {
            Coast::Wing => Coast::Wing,
            Coast::Land => Coast::Land,
            Coast::Single => Coast::Single,
            Coast::North => Coast::Single,
            Coast::South => Coast::Single,
            Coast::East => Coast::Single,
            Coast::West => Coast::Single,
        }
    }


}