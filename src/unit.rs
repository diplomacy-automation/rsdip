#[derive(PartialEq, Debug)]
enum Unit {
    Fleet,
    Army,
}

impl Unit {
    fn parse(input: &str) -> Unit {
        match input.to_lowercase().as_str() {
            "fleet" => Unit::Fleet,
            "army" => Unit::Army,
            _ => panic!("{}", input)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::unit::Unit;

    #[test]
    fn paser_unit() {
        assert_eq!(Unit::parse("fleet"), Unit::Fleet);
        assert_eq!(Unit::parse("Fleet"), Unit::Fleet);
        assert_eq!(Unit::parse("army"), Unit::Army);
        assert_eq!(Unit::parse("Army"), Unit::Army);
    }
    #[test]
    #[should_panic]
    fn parse_unit_panic() {
        Unit::parse("Foo");
    }

}

