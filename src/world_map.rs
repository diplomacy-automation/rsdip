use std::collections::{BTreeMap, HashMap, HashSet};
use std::fs;

use amxml::dom::new_document;
use petgraph::Graph;

use crate::coast::Coast;

#[derive(Clone, Hash, Eq, PartialEq)]
struct Location {
    pub province: Province,
    pub coast: Coast,
}

impl Location {
    pub fn new(province: Province, coast: Coast) -> Location {
        Location { province, coast }
    }
}


#[derive(Clone, PartialEq, Eq, Hash, Debug)]
struct Province {
    pub full_name: String,
    pub short_name: String,
}

impl Province {
    pub fn new(full_name: String, short_name: String) -> Province {
        Province { full_name, short_name }
    }
}

struct WorldMap {
    province_map: BTreeMap<String, Province>,
    graph: Graph<Location, ()>,
}


impl WorldMap {
    pub fn new() -> Self {
        let content = fs::read_to_string("resources/std_adjacency.xml").unwrap();
        let doc = new_document(content.as_str()).unwrap();
        let result = doc.get_nodeset("/PROVINCES/PROVINCE").unwrap();
        let mut province_map: BTreeMap<String, Province> = BTreeMap::new();
        province_map.extend(
            result.iter().flat_map(|n| {
                let unique_names = n.get_nodeset("UNIQUENAME").unwrap();
                let short_name = n.attribute_value("fullname").unwrap();
                let full_name = n.attribute_value("shortname").unwrap();
                let p = Province::new(short_name.clone(), full_name.clone());
                let mut vn = vec![short_name, full_name];
                vn.extend(unique_names.iter().map(|n| n.attribute_value("name").unwrap()));
                return vn.into_iter().map(move |n| (n, p.clone()));
            }));
        let edges: Vec<(Location, Location)> = result.iter().flat_map(|p| {
            let adjs = p.get_nodeset("ADJACENCY").unwrap();
            adjs.into_iter().flat_map(|ad| {
                let refs = ad.attribute_value("refs").unwrap();
                refs.split(" ").into_iter().map(|s| {
                    let coast = ad.attribute_value("type")
                        .and_then(|it| Coast::parse(it.as_str())).unwrap();
                    let province = p.attribute_value("shortname")
                        .and_then(|it| province_map.get(it.as_str())).unwrap();
                    let a: Vec<&str> = s.split("-").collect();
                    (Location::new(province.clone(), coast.clone()),
                     Location::new(a.get(0)
                                       .and_then(|&it| province_map.get(&it.to_string()))
                                       .unwrap().clone(),
                                   if a.len() == 1 {
                                       coast.default_coast()
                                   } else {
                                       a.get(1).and_then(|&it| Coast::parse(it)).expect("hoge")
                                   }))
                }).collect::<Vec<_>>()
            }).collect::<Vec<_>>()
        }).collect();
        let locations = edges.iter().flat_map(|(l0, l1)| {
            let v = vec![l0.clone(), l1.clone()];
            v.into_iter()
        }).collect::<HashSet<_>>();
        let mut graph = Graph::<Location, ()>::new();
        let mut map = HashMap::<Location, _>::new();
        locations.into_iter().for_each(|l| {
            map.insert(l.clone(), graph.add_node(l));
        });
        edges.iter().for_each(|(l0,l1)|{
            graph.add_edge(*map.get(l0).unwrap(), *map.get(l1).unwrap(),());
        });
//        let mut graph= Graph::from_edges(edges);

        return WorldMap {province_map, graph };
    }

    pub fn size(&self) -> usize {
        self.province_map.values().collect::<HashSet<_>>().len()
    }

    fn province(&self) {}

    fn location(&self) {}

    fn find_connected(&self) {}

    fn is_neighbour(&self) {}

    fn is_reachable(&self) {}

    fn can_convoy(&self) {}

    fn valid_convoy(&self) {}

    fn neighbours(&self) {}

    fn distance(&self) {}

    fn exists(&self) {}
}

#[cfg(test)]
mod tests {
    use std::collections::{BTreeMap, HashSet};

    use petgraph::Graph;
    use rspec;

    use crate::world_map::WorldMap;
    use petgraph::graphmap::GraphMap;
    use std::rc::Rc;
    use petgraph::prelude::StableGraph;

    #[test]
    fn set() {
        #[derive(Hash, PartialEq, Eq, Debug)]
        struct S {
            k: String
        };
        let mut s = HashSet::<S>::new();
        s.insert(S { k: "1".to_string() });
        s.insert(S { k: "1".to_string() });
        assert_eq!(1, s.len());
    }

    #[test]
    fn test() {
        let mut m = BTreeMap::new();
        let k = "k".to_string();
        let k2 = "k2".to_string();
        let v = "v".to_string();
        m.insert(k, &v);
        m.insert(k2, &v);
    }

    #[test]
    fn size() {
        let world_map = WorldMap::new();
        assert_eq!(76, world_map.size());
    }
    #[test]
    fn privince() {
//        worldMap.province("London").shortName === "lon"
    }

    #[test]
    fn main() {
        #[derive(Clone, Default, Debug)]
        struct Environment {}
        rspec::run(&rspec::describe("opens a suite", Environment::default(), |ctx| {
            ctx.specify("opens a context", |ctx| {
                for i in 1..10 {
                    let msg = format!("opens an example {}", i.clone());
                    ctx.it(Box::leak(msg.into_boxed_str()), move |_| i < 10);
                }
            });
        }));
    }

    #[test]
    fn petgraph() {
        #[derive(Eq, PartialEq, Debug, Clone, Default, Hash, PartialOrd, Ord)]
        struct S {
            k: String,
        }
        {
            let s1 = S { k: "1".to_string() };
            let s12 = S { k: "1".to_string() };
            assert_eq!(s1, s12);
            let mut g = Graph::<S, ()>::new();
            let n = g.add_node(S { k: "aaa".to_string() });
            let n1 = g.add_node(s1);
            let n2 = g.add_node(s12);
//            assert_eq!(n1, n2);
        }
        {
            let s1 = Rc::new(S { k: "1".to_string() });
            let s12 = s1.clone();
            assert_eq!(s1, s12);
            let mut g = Graph::<Rc<S>, ()>::new();
            let n1 = g.add_node(s1.clone());
            let n2 = g.add_node(s1.clone());
            assert_eq!(s1.clone(), s1.clone());
            assert_ne!(n1, n2);
        }
        {
            let s0 = S { k: "0".to_string() };
            let s1 = S { k: "1".to_string() };
            let s12 = S { k: "1".to_string() };
            let s3 = S { k: "3".to_string() };
            let mut g = Graph::<S, ()>::new();
            let n0 = g.add_node(s0);
            let n1 = g.add_node(s1.clone());
//            let n1_ = g.add_node(s1);
            let n12 = g.add_node(s12);
            g.extend_with_edges(&[(n0, n1)]);
            assert_eq!(3, g.node_count());
        }
        {
            let s0 = S { k: "0".to_string() };
            let s1 = S { k: "1".to_string() };
            let s12 = S { k: "1".to_string() };
            let s3 = S { k: "3".to_string() };
            let mut g = StableGraph::<S, ()>::new();
//            GraphMap::from_edges(&[(s0,s1)]);
//            let mut g = GraphMap::new();
            g.add_node(s0);
            g.add_node(s1);
            g.add_node(s12);
            assert_eq!(3, g.node_count());
//            let g = StableGraph::<S, ()>::from_edges(&[(s0, s1)]);
        }
//        let g = Graph::<S,()>::from_edges(&[(s0,s1)]);
//        let z = Graph::<(), i32>::from_edges(&[(1, 2), (3, 4)]);
//        let s = Graph::from_edges(&[("a","b"), ("a","c")]);
//        let mut graph= Graph::from_edges(edges);
    }
}